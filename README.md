# Test game

### Purposes

This test game was written on Rust, to learn leanguage and have a funny time.

## Development

### The Story 

* Game has a hero **H**

* The Hero tries to save a princess **P**

* The princess was captured by evil sorcerer **S**

* The sorcerer holds the princess in his castle in a hign tower

* To reach the princess hero need to free the castle from sorcerer\`s monsters **M** and find the way to the tower

* The only entrance to the castle lies through the dungeon

### Levels

#### Dungeons

1. First room

2. Second room

3. Third room

#### Castle

1. First room

2. Second room

3. Third room

#### Final round

1. Fight with evil sorcerer

2. Save the princess
